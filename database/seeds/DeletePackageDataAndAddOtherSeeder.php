<?php

use App\Package;
use Illuminate\Database\Seeder;

class DeletePackageDataAndAddOtherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::truncate();
        Package::insert(
            [
                [
                    'package_id' => 'TRANSAT_CABLE_100_44',
                    'package_url_en' => 'https://form.innomobile.ca/en/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/cable',
                    'package_isp' => 'Transat',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 100 Mbps',
                    'package_email' => '',
                    'package_price' => '44.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'TRANSAT_CABLE_60_39',
                    'package_url_en' => 'https://form.innomobile.ca/en/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/cable',
                    'package_isp' => 'Transat',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 60 Mbps',
                    'package_email' => '',
                    'package_price' => '39.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'TRANSAT_CABLE_30_34',
                    'package_url_en' => 'https://form.innomobile.ca/en/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/cable',
                    'package_isp' => 'Transat',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 30 Mbps',
                    'package_email' => '',
                    'package_price' => '34.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'TRANSAT_DSL_50_34',
                    'package_url_en' => 'https://form.innomobile.ca/en/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/dsl',
                    'package_isp' => 'Transat',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 50 Mbps',
                    'package_email' => '',
                    'package_price' => '34.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'TRANSAT_DSL_15_29',
                    'package_url_en' => 'https://form.innomobile.ca/en/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/dsl',
                    'package_isp' => 'Transat',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 15 Mbps',
                    'package_email' => '',
                    'package_price' => '29.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'TRANSAT_DSL_10_24',
                    'package_url_en' => 'https://form.innomobile.ca/en/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/dsl',
                    'package_isp' => 'Transat',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 10 Mbps',
                    'package_email' => '',
                    'package_price' => '24.95',
                    'currency' => '$',
                    'login' => 'innomobile2018',
                    'password' => 'Innomobile2020'
                ],
                [
                    'package_id' => 'CARRYTEL_CABLE_60_44',
                    'package_url_en' => 'https://form.innomobile.ca/en/carrytel/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/carrytel/cable',
                    'package_isp' => 'Carrytel',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 60 Mbps',
                    'package_email' => '',
                    'package_price' => '44.99',
                    'currency' => '$',
                    'login' => 'Shawn.jose.lucien@gmail.com',
                    'password' => '50015469'
                ],
                [
                    'package_id' => 'CARRYTEL_CABLE_30_34',
                    'package_url_en' => 'https://form.innomobile.ca/en/carrytel/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/carrytel/cable',
                    'package_isp' => 'Carrytel',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 30 Mbps',
                    'package_email' => '',
                    'package_price' => '34.99',
                    'currency' => '$',
                    'login' => 'Shawn.jose.lucien@gmail.com',
                    'password' => '50015469'
                ],
                [
                    'package_id' => 'CARRYTEL_CABLE_15_29',
                    'package_url_en' => 'https://form.innomobile.ca/en/carrytel/cable',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/carrytel/cable',
                    'package_isp' => 'Carrytel',
                    'package_type' => 'Cable',
                    'package_name' => 'Cable 15 Mbps',
                    'package_email' => '',
                    'package_price' => '29.99',
                    'currency' => '$',
                    'login' => 'Shawn.jose.lucien@gmail.com',
                    'password' => '50015469'
                ],
                [
                    'package_id' => 'CARRYTEL_DSL_50_34',
                    'package_url_en' => 'https://form.innomobile.ca/en/carrytel/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/carrytel/dsl',
                    'package_isp' => 'Carrytel',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 50 Mbps',
                    'package_email' => '',
                    'package_price' => '34.99',
                    'currency' => '$',
                    'login' => 'Shawn.jose.lucien@gmail.com',
                    'password' => '50015469'
                ],
                [
                    'package_id' => 'CARRYTEL_DSL_15_29',
                    'package_url_en' => 'https://form.innomobile.ca/en/carrytel/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/carrytel/dsl',
                    'package_isp' => 'Carrytel',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 15 Mbps',
                    'package_email' => '',
                    'package_price' => '29.99',
                    'currency' => '$',
                    'login' => 'Shawn.jose.lucien@gmail.com',
                    'password' => '50015469'
                ],
                [
                    'package_id' => 'VIRGIN_DSL_100_50',
                    'package_url_en' => 'https://form.innomobile.ca/en/virgin/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/virgin/dsl',
                    'package_isp' => 'Virgin',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 100 Mbps',
                    'package_email' => 'virgin@innomobile.ca',
                    'package_price' => '50.00',
                    'currency' => '$',
                    'login' => 'login',
                    'password' => 'password'
                ],
                [
                    'package_id' => 'VIRGIN_DSL_50_40',
                    'package_url_en' => 'https://form.innomobile.ca/en/virgin/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/virgin/dsl',
                    'package_isp' => 'Virgin',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 50 Mbps',
                    'package_email' => 'virgin@innomobile.ca',
                    'package_price' => '40.00',
                    'currency' => '$',
                    'login' => 'login',
                    'password' => 'password'
                ]
            ]
        );
    }
}
