<?php

use Illuminate\Database\Seeder;
use App\User;

class ChangeUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::where("email", "webihub@gmail.com")->update(["email" => "info@innomobile.ca", "password" => Hash::make("Allo99")]);
    }
}
