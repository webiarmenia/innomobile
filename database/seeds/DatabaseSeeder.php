<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();
         $this->call(UsersTablesSeeder::class);
         $this->call(ChangeUsersTableSeeder::class);
         $this->call(ChangePackagesTransatTableSeeder::class);
         $this->call(ChangePackagesTransatEmailTableeSeeder::class);
         $this->call(DeletePackageDataAndAddOtherSeeder::class);
         $this->call(ChangeVirginPackagesSeeder::class);
         $this->call(AddBackIdPackageSeeder::class);
         $this->call(ChangeOnePackageBackIdSeeder::class);
    }
}
