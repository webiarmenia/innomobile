<?php

use Illuminate\Database\Seeder;
use App\Package;


class ChangePackagesTransatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $array = array(
            array(
                "package_price" => "34.95",
                "package_id" => "TRANSAT_CABLE_30_34",
                "package_name" => "Cable 30 Mbps"
            ),
            array(
                "package_price" => "39.95",
                "package_id" => "TRANSAT_CABLE_60_39",
                "package_name" => "Cable 60 Mbps"
            ),
            array(
                "package_price" => "44.95",
                "package_id" => "TRANSAT_CABLE_100_44",
                "package_name" => "Cable 100 Mbps"
            ),
            array(
                "package_price" => "34.95",
                "package_id" => "TRANSAT_DSL_50_35",
                "package_name" => "DSL 50 Mbps",
                "package_backup_id" => "TRANSAT_CABLE_100_44"
            ),
            array(
                "package_price" => "29.95",
                "package_id" => "TRANSAT_DSL_15_29",
                "package_name" => "DSL 15 Mbps",
                "package_backup_id" => "TRANSAT_CABLE_60_369"
            ),
            array(
                "package_price" => "24.95",
                "package_id" => "TRANSAT_DSL_10_24",
                "package_name" => "DSL 10 Mbps",
                "package_backup_id" => "TRANSAT_CABLE_30_34"
            ),
        );
        $packagesTransat = Package::where("package_isp", "Transat")->get();
        foreach ($packagesTransat as $key => $packageTransat) {
            $packagesTransat[$key]['package_price'] = $array[$key]['package_price'];
            $packagesTransat[$key]['package_id'] = $array[$key]['package_id'];
            $packagesTransat[$key]['package_name'] = $array[$key]['package_name'];
            $packagesTransat[$key]->update();
        }

        Package::where("package_id", "TRANSAT_DSL_50_35")->update(["package_id" => "TRANSAT_DSL_50_34"]);
    }
}