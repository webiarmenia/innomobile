<?php

use Illuminate\Database\Seeder;
use App\Package;

class ChangePackagesTransatEmailTableeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::where("package_isp", "Transat")->update(["package_email" => "pour-transat@innomobile.ca"]);
    }
}
