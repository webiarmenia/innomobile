<?php

use Illuminate\Database\Seeder;
use App\Package;

class ChangeVirginPackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::where("package_isp", "Virgin")->update(["old" => true]);
        Package::insert(
            [
                [
                    'package_id' => 'VIRGIN_DSL_100_60',
                    'package_url_en' => 'https://form.innomobile.ca/en/virgin/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/virgin/dsl',
                    'package_isp' => 'Virgin',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 100 Mbps',
                    'package_email' => 'virgin@innomobile.ca',
                    'package_price' => '60.00',
                    'currency' => '$',
                    'login' => 'login',
                    'password' => 'password'
                ],
                [
                    'package_id' => 'VIRGIN_DSL_50_50',
                    'package_url_en' => 'https://form.innomobile.ca/en/virgin/dsl',
                    'package_url_fr' => 'https://form.innomobile.ca/fr/virgin/dsl',
                    'package_isp' => 'Virgin',
                    'package_type' => 'DSL',
                    'package_name' => 'DSL 50 Mbps',
                    'package_email' => 'virgin@innomobile.ca',
                    'package_price' => '50.00',
                    'currency' => '$',
                    'login' => 'login',
                    'password' => 'password'
                ]
            ]
        );
    }
}
