<?php

use Illuminate\Database\Seeder;
use App\Package;

class ChangeOnePackageBackIdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Package::where('back_id', 21)->update(['back_id' => 27]);
    }
}
