<?php

use Illuminate\Database\Seeder;
use App\Package;

class AddBackIdPackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $packages = Package::where('package_isp', '!=', 'Virgin')->get();

        $backIds = [35, 33, 32, 34, 24, 21, 200000136, 200000136, 200000136, 200000137, 200000136];
        foreach($packages as $key => $package){
            $package->back_id = $backIds[$key];
            $package->save();
        }
    }
}
