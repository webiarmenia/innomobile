<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->unique();
            $table->string('language');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('appartment_number');
            $table->string('street_number');
            $table->string('street_name');
            $table->string('city');
            $table->string('region_name');
            $table->string('postal_code');
            $table->string('phone_number');
            $table->string('package_of_interest');
            $table->string('package_status');
            $table->string('dsl_eligible')->default('Unknown');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}