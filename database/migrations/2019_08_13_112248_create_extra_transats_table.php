<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraTransatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_transats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('daterendivo');
            $table->date('daterendivo1');
            $table->date('daterendivo2');
            $table->smallInteger('priode');
            $table->smallInteger('priode1');
            $table->smallInteger('priode2');
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extra_transats', function (Blueprint $table) {
            $table->dropForeign('transat_extra_customer_id_foreign');
            $table->dropColumn('customer_id');
        });
        Schema::dropIfExists('');
    }
}
