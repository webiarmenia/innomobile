<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('package_id')->unique()->nullable(false);
            $table->string('package_url_en')->nullable(false);
            $table->string('package_url_fr')->nullable(false);
            $table->string('package_isp')->nullable(false);
            $table->string('package_name')->nullable(false);
            $table->string('package_email');
            $table->float('package_price');
            $table->string('currency',20)->default('$');
            $table->text('package_description_en');
            $table->text('package_description_fr');
            $table->string('package_backup_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
