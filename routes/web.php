<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    $prev_url = url()->previous();
    if($locale == 'en'){
        $new_url = str_replace('fr' , $locale, $prev_url);
    }else if($locale == 'fr'){
        $new_url = str_replace('en' , $locale, $prev_url);
    }
    return redirect($new_url);
});

// Authentication Routes...
Route::get('innomobile_super_admin/login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

Route::group(['middleware' => 'admin'], function () {
    Route::prefix('innomobile_super_admin')->group(function () {
        Route::get('/', function (){
            return redirect()->route('admin.customers');
        });

        Route::get('/customers', 'CustomerController@index')->name('admin.customers');
        Route::get('/customer/edit/{id}/{isp}', 'CustomerController@edit')->name('admin.customer.edit');
        Route::post('/customer/update/{id}', 'CustomerController@update')->name('admin.customerData.update');
        Route::get('/customer/show/{id}/{isp}', 'CustomerController@show');
        Route::post('/customer/destroy/{id}', 'CustomerController@destroy')->name('admin.customer.destroy');

        Route::get('/cronJobs', 'OrderController@index')->name('admin.cronJobs');
        Route::post('/cronJobsThose', 'OrderController@index')->name('admin.cron.index');

        Route::post('cron', 'OrderController@update');

        Route::get('/customer/export', 'DownloadCustomerController@csv_export')->name('export');

        Route::get('/packages', 'PackageController@index')->name('admin.packages');
        Route::get('/edit/{id}', 'PackageController@edit')->name('admin.package.edit');
        Route::put('/update/{id}', 'PackageController@update')->name('admin.package.update');

    });
});

Route::get('/', function(){
    return redirect('/' . app()->getLocale() .'/a/dsl');
});

Route::get('/{prefix}', function($prefix){
    if($prefix == 'dsl'){
        return redirect('/en/a/dsl');
    }else if($prefix == 'cable'){
        return redirect('/en/a/cable');
    }else if($prefix == 'en'){
        return redirect('/en/a/dsl');
    }else if($prefix == 'fr'){
        return redirect('/fr/a/dsl');
    }
});
// Transat
Route::get('/{lang}/a/{package_type}', 'TransatController@create')->name('create.customer');
Route::post('{lang}/a/{package_type}/customer/store', 'TransatController@store');
// Carrytel
Route::get('/{lang}/b/{package_type}', 'CarrytelCotnroller@create')->name('create.carrytel.order');
Route::post('{lang}/b/{package_type}/customer/store/', 'CarrytelCotnroller@store');
// Virgin
Route::get('/{lang}/c/{package_type}', 'VirginController@create')->name('create.virgin.order');
Route::post('{lang}/c/{package_type}/customer/store/', 'VirginController@store');
