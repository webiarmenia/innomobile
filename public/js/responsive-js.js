$(".have-sub-menu").mouseover(function(){
    $(".have-sub-menu a span").css("color", "#dd9933");
});

$(".have-sub-menu").mouseout(function(){

    $(".have-sub-menu a span").css("color", "#fff");
});


$(".header-menu-in").mouseover(function(){
    $(".menu-show ul li").css("border", "1px solid #00549a");
});

$(".header-menu-in").mouseout(function(){
    $(".menu-show ul li").css("border", "1px solid #fff");
});
$(".responsive-sub-menu-down").click(function(){
    if($(".sub-menu-item").hasClass("active-sub-menu")){
        $(".sub-menu-item").css("height", "0");
        $(".sub-menu-item").css("margin-top", "0 ");
        $(".wi-li").css("margin-top", "0");
        $(".sub-menu-item").removeClass("active-sub-menu");
        $(".have-sub-menu span i").css("transform", "rotate(0deg)");
        $(".responsive-sub-menu-down").css("left", "138px");
    }else{
        $(".sub-menu-item").css("height", "248px");
        $(".sub-menu-item").css("margin-top", "7px");
        $(".wi-li").css("margin-top", "263px");
        $(".sub-menu-item").addClass("active-sub-menu");
        $(".have-sub-menu span i").css("transform", "rotate(180deg)");
        $(".responsive-sub-menu-down").css("left", "134px");
    }
});