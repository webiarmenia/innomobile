<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CronJob extends Model
{
    protected $table = 'cron_job';


    protected $fillable = [
        'customer_id',
        'status',
        'worked_time'
    ];

    public function customer()
    {
        return $this->hasOne(Customer::class,'id','customer_id');
    }


}
