<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class SendEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $package;
    public $link;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$package,$link)
    {
        $this->customer = $customer;
        $this->package = $package;
        $this->link = $link;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $package = $this->package;
        $customer = $this->customer;
        $link = $this->link;

        return $this->view('emails.sendEmail',compact('package','customer','link'));
    }

}

