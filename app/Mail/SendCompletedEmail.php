<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCompletedEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $package;
    public $dateAprox;
    public $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$package,$link,$dateAprox)
    {
        $this->customer = $customer;
        $this->package = $package;
        $this->link = $link;
        $this->dateAprox = $dateAprox;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $package = $this->package;
        $customer = $this->customer;
        $dateAprox = $this->dateAprox;
        $link = $this->link;

        return $this->view('emails.sendCompletedEmail',compact('package','customer','link','dateAprox'));
    }
}
