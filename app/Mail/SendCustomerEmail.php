<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCustomerEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $customer;
    public $packageBackup;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$packageBackup)
    {
        $this->customer = $customer;
        $this->packageBackup = $packageBackup;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $packageBackup = $this->packageBackup;
        $customer = $this->customer;

        return $this->view('emails.sendCustomerEmail',compact('packageBackup','customer'));
    }
}
