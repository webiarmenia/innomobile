<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCustomerEmailYes extends Mailable
{

    use Queueable, SerializesModels;
    public $customer;
    public $package;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$package)
    {
        $this->customer = $customer;
        $this->package = $package;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $customer = $this->customer;
        $package = $this->package;

        return $this->view('emails.sendCustomerEmailYes',compact('customer','package'));
    }

}
