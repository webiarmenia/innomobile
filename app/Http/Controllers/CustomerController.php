<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App;
use App\Package;
use App\Customer;
use App\ExtraTransat;
use App\CronJob;

class CustomerController extends Controller
{
    /**
     * Display a listing all customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::latest()->with('package')->paginate(10);
        return view('admin/customer/index', compact('customers'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $isp)
    {
        if ($isp == 'Carrytel' || $isp == 'Transat' || $isp == 'Virgin') {
            $customer = Customer::where("id", $id)->firstOrfail();
        } else {
            abort(404);
        }
        return view('admin/customer/show')->with(['customer' => $customer]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $isp)
    {
        if ($isp == "Transat") {
            $customer = Customer::where("id", $id)->with($isp)->firstOrfail();
            $arrayDate = [];
            $arrayDateEdit = [];
            if ($customer->Transat != null) {
                array_push($arrayDate, $customer->Transat->daterendivo, $customer->Transat->daterendivo1, $customer->Transat->daterendivo2);
                foreach ($arrayDate as $key => $value) {
                    $date = explode("-", $value);
                    $date = $date[1] . "/" . $date[2] . "/" . $date[0];
                    array_push($arrayDateEdit, $date);
                }
            }
        } else {
            $customer = Customer::where("id", $id)->firstOrfail();
        }
        $packages = Package::where("package_isp", $isp)->get();
        if ($isp == "Transat") {
            return view('admin.customer.edit' . $isp)->with(['customer' => $customer, 'packages' => $packages, 'arrayDateEdit' => $arrayDateEdit]);
        } else {
            return view('admin.customer.edit' . $isp)->with(['customer' => $customer, 'packages' => $packages]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required | string | unique:customers,email,' . $id,
            'language' => ['required', 'string'],
            'title' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'appartment_number' => ['nullable', 'regex:/^[A-Z0-9]*((-|\s)*[A-Z0-9])*$/'],
            'street_number' => ['required', 'string'],
            'street_name' => ['required', 'string'],
            'city' => ['required', 'string'],
            'region_name' => ['required', 'string'],
            'postal_code' => ['required', 'postal_code' => 'postal_code:CA', 'string'],
            'phone_number' => ['required', 'string'],
            'package_of_interest' => ['required', 'string'],
            'package_status' => ['required', 'string'],
            'dsl_eligible' => ['required', 'string']
        ]);

        $customer = Customer::where('id', $id)->firstOrfail();

        $customer->email = $request->email;
        $customer->language = $request->language;
        $customer->title = $request->title;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->appartment_number = $request->appartment_number;
        $customer->street_number = $request->street_number;
        $customer->street_name = $request->street_name;
        $customer->city = $request->city;
        $customer->region_name = $request->region_name;
        $customer->postal_code = $request->postal_code;
        $customer->phone_number = $request->phone_number;
        $customer->package_of_interest = $request->package_of_interest;
        $customer->package_status = $request->package_status;
        $customer->dsl_eligible = $request->dsl_eligible;

        $customer->save();

        return redirect()->route('admin.customers')->with('success', 'Customer data successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->type == 'Ciktel') {
            $extraCiktel = ExtraCiktel::where("customer_id", $id)->first();
            if (isset($extraCiktel)) {
                $extraCiktel->delete();
            }
        } else if ($request->type == 'Transat') {
            $extraTransat = ExtraTransat::where("customer_id", $id)->first();
            if (isset($extraTransat)) {
                $extraTransat->delete();
            }
        }
        $cronJob = CronJob::where("customer_id", $id)->first();
        if (isset($cronJob)) {
            $cronJob->delete();
        }
        Customer::where("id", $id)->delete();

        return back()->with('success', 'Customer data successfully deleted');

    }
}
