<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Customer;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::where("old", false)->paginate(10);
        return view('admin.package.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package = Package::find($id);

        return view('admin.package.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'package_id' => 'required|string',
            'package_name' => 'required|string',
            'package_type' => 'required|string|in:DSL,Cable',
            'package_price' => 'required',
            'package_email' => 'required_if:package_isp,Virgin|email',
            'login' => 'required',
            'password' => 'required'
        ]);

        if($request->selected){
            $selected = true;
        } else {
            $selected = false;
        }
        $oldPackageId = false;
        $package = Package::where("id", $id)->firstOrFail();
        if($request->package_id != $package->package_id){
            $oldPackageId = $package->package_id;
        }
        $package->package_id = $request->package_id;
        $package->package_name = $request->package_name;
        $package->package_type = $request->package_type;
        $package->package_price = $request->package_price;
        $package->package_email = $request->package_email;
        $package->login = $request->login;
        $package->password = $request->password;
        $package->selected = $selected;
        $package->save();
        if($oldPackageId) {
            Customer::where("package_of_interest", $oldPackageId)->update(["package_of_interest" => $package->package_id]);
        }
        if($package->selected){
            Package::where("package_type", $package->package_type)->where("package_isp", $package->package_isp)->where("id", "!=", $package->id)->update(['selected' => false]);
        }

        return redirect()->route('admin.packages')->with('success', 'Package data successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
