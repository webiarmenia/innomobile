<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Requests\Virgin\StoreRequest;
use Illuminate\Support\Facades\App;

class VirginController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang, $package_type)
    {
        $package_type_upper = strtoupper($package_type);
        App::setLocale($lang);
        $isp = "Virgin";
        $packages = Package::where("package_isp", $isp)->where("package_type", $package_type_upper)->where("old", false)->get()->toArray();
        $removeSelectHeading = 'DSL';
        $package = Package::where("selected", true)->where("package_type", $removeSelectHeading)->where("package_isp",$isp)->first();
        $selectedPackageId = null;
        if ($package) {
            $selectedPackageId = $package->package_id;
        }
        if (!empty($packages)) {
            return view('innomobile.customerVirgin')->with([
                'packages' => $packages,
                'isp' => $isp,
                'lang' => $lang,
                'package_type' => $package_type,
                'remove_select_heading' => $removeSelectHeading,
                'selectedPackageId' => $selectedPackageId,
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, $lang, $package_type)
    {
        $customer = new Customer();

        $customer->email = $request->email;
        $customer->language = $lang;
        $customer->title = $request->title;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->street_number = $request->street_number;
        $customer->street_name = $request->street_name;
        $customer->city = $request->city;
        $customer->region_name = 'Quebec';
        $customer->postal_code = $request->postal_code;
        $customer->phone_number = $request->phone_number;
        $customer->package_of_interest = $request->package_of_interest;
        $customer->package_status = 'Ordered';
        $customer->dsl_eligible = 'Unknown';

        $customer->save();

        $package = Package::where("package_id", $request->package_of_interest)->firstOrFail();

        $virginEmail = $package->package_email;
        $innomobileEmail = env('MAIL_USERNAME');
        \Mail::send('emails.sendEmailToAgentsAtVirgin', ['package' => $package, 'customer' => $customer], function($message) use($innomobileEmail, $virginEmail){
            $message->to($virginEmail)->subject('New order');
            $message->from($innomobileEmail, 'Innomobile');
        });

        $orderEmail = 'order@innomobile.ca';
        \Mail::send('emails.sendCompletedEmail', ['package' => $package, 'customer' => $customer, 'dateAprox' => null], function($message) use($innomobileEmail, $orderEmail){
            $message->to($orderEmail)->subject('New order');
            $message->from($innomobileEmail, 'Innomobile');
        });
        if ($lang == 'en') {
            return redirect('https://innomobile.ca/en/promotion-thank-you-b/');
        } else if ($lang == 'fr') {
            return redirect('https://innomobile.ca/fr/promotion-merci-b/');
        }
    }
}
