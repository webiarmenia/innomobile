<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class MainController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMessage()
    {
        return view('admin.isp.index');
    }

    public function index()
    {
        return view('admin.user.login');
    }

}
