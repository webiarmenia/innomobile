<?php

namespace App\Http\Controllers;

use App;
use App\CronJob;
use App\Customer;
use App\Package;
use Illuminate\Http\Request;
use App\Http\Requests\Carrytel\StoreRequest;

class CarrytelCotnroller extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang, $package_type)
    {
        App::setLocale($lang);
        $isp = "Carrytel";
        if ($package_type === 'dsl') {
            $packages = Package::where("package_isp", $isp)->where("package_type", "DSL")->get()->toArray();
            $removeSelectHeading = 'DSL';
        } else if ($package_type === 'cable') {
            $packages = Package::where("package_isp", $isp)->where("package_type", "Cable")->get()->toArray();
            $removeSelectHeading = 'Cable';
        } else {
            return App::abort(404);
        }
        $package = Package::where("selected", true)->where("package_type", $removeSelectHeading)->where("package_isp", $isp)->first();
        $selectedPackageId = null;
        if ($package) {
            $selectedPackageId = $package->package_id;
        }

        if (!empty($packages)) {
            return view('innomobile.customerCarrytel')->with([
                'packages' => $packages,
                'isp' => $isp,
                'lang' => $lang,
                'package_type' => $package_type,
                'remove_select_heading' => $removeSelectHeading,
                'selectedPackageId' => $selectedPackageId
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, $lang, $package_type)
    {
        $customer = new Customer();

        $customer->email = $request->email;
        $customer->language = $lang;
        $customer->title = $request->title;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->street_number = $request->street_number;
        $customer->street_name = $request->street_name;
        $customer->city = $request->city;
        $customer->region_name = 'Quebec';
        $customer->postal_code = $request->postal_code;
        $customer->phone_number = $request->phone_number;
        $customer->package_of_interest = $request->package_of_interest;
        $customer->package_status = 'Ordered';
        if ($package_type == 'dsl') {
            $customer->dsl_eligible = 'Unknown';
        } else if ($package_type == 'cable') {
            $customer->dsl_eligible = 'No';
        } else {
            abort(404);
        }

        $customer->save();

        $cronJob = new CronJob();
        $cronJob->customer_id = $customer->id;
        $cronJob->package_isp = "Carrytel";
        $cronJob->save();

        if ($lang == 'en') {
            return redirect('https://innomobile.ca/en/promotion-thank-you-b/');
        } else if ($lang == 'fr') {
            return redirect('https://innomobile.ca/fr/promotion-merci-b/');
        }
    }

}
