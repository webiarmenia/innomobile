<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\ExtraTransat;
use App\ExtraCiktel;
use App\Package;
use App\Customer;
use App\CronJob;
use Carbon\Carbon;

class OrderController extends Controller
{

    /**
     * Display a listing all customers.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->those_worked_time) && $request->those_worked_time != null ){
            $selected = $request->those_worked_time;
            $cronJobs = CronJob::where("worked_time", $request->those_worked_time)->latest()->paginate(10);
        }else{
            $selected = 10;
            $cronJobs = CronJob::latest()->paginate(10);
        }
        return view('admin/cron/index', compact('cronJobs','selected'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        CronJob::where("id", $_POST["id"])->update([
            'worked_time' => 0
        ]);
    }

}
