<?php

namespace App\Http\Controllers;

use App\CronJob;
use App\Customer;
use App\ExtraTransat;
use App\Package;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App;

class TransatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($lang, $package_type)
    {
        App::setLocale($lang);
        $isp = 'Transat';
        if ($package_type === 'dsl') {
            $packages = Package::where("package_isp", $isp)->where("package_type", "DSL")->get()->toArray();
            $removeSelectHeading = 'DSL';
        } else if ($package_type === 'cable') {
            $packages = Package::where("package_isp", $isp)->where("package_type", "Cable")->get()->toArray();
            $removeSelectHeading = 'Cable';
        } else {
            return App::abort(404);
        }
        $package = Package::where("selected", true)->where("package_type", $removeSelectHeading)->where("package_isp",$isp)->first();
        $selectedPackageId = null;
        if ($package) {
            $selectedPackageId = $package->package_id;
        }
        if (!empty($packages)) {
            return view('innomobile.customer' . $isp)->with([
                'packages' => $packages,
                'isp' => $isp,
                'lang' => $lang,
                'package_type' => $package_type,
                'remove_select_heading' => $removeSelectHeading,
                'selectedPackageId' => $selectedPackageId,
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $lang, $package_type)
    {

        $this->validate($request, [
            'email' => ['required', 'string', 'unique:customers'],
            'title' => ['required', 'string'],
            'first_name' => ['required', 'string'],
            'last_name' => ['required', 'string'],
            'appartment_number' => ['nullable', 'regex:/^[A-Z0-9]*((-|\s)*[A-Z0-9])*$/'],
            'street_number' => ['required'],
            'street_name' => ['required', 'string'],
            'city' => ['required', 'string'],
            'region_name' => ['required'],
            'postal_code' => ['required', 'postal_code' => 'postal_code:CA', 'string'],
            'phone_number' => ['required', 'regex:/^[+0-9]*((-|\s)*[+0-9])*$/'],
            'package_of_interest' => ['required', 'string'],
            'daterendivo' => 'required|date_format:"m/d/Y"',
            'daterendivo1' => 'required|date_format:"m/d/Y"',
            'daterendivo2' => 'required|date_format:"m/d/Y"',
            'priode' => 'required|numeric|min:2|max:5',
            'priode1' => 'required|numeric|min:2|max:5',
            'priode2' => 'required|numeric|min:2|max:5',
        ]);

        $customer = new Customer();
        echo $request->priode . '</br>';
        echo $request->priode1 . '</br>';
        echo $request->priode2 . '</br>';
        $customer->email = $request->email;
        $customer->language = $lang;
        $customer->title = $request->title;
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->appartment_number = $request->appartment_number;
        $customer->street_number = $request->street_number;
        $customer->street_name = $request->street_name;
        $customer->city = $request->city;
        $customer->region_name = 'Quebec';
        $customer->postal_code = $request->postal_code;
        $customer->phone_number = $request->phone_number;
        $customer->package_of_interest = $request->package_of_interest;

        if ($package_type == 'dsl') {
            $customer->dsl_eligible = 'Unknown';
        } else if ($package_type == 'cable') {
            $customer->dsl_eligible = 'No';
        } else {
            abort(404);
        }

        $customer->save();
        $arrayDate = [];
        array_push($arrayDate, $request->daterendivo, $request->daterendivo1, $request->daterendivo2);
        $daterendivo = '';
        $daterendivo1 = '';
        $daterendivo2 = '';
        foreach ($arrayDate as $key => $value) {
            $date = explode("/", $value);
            $date = $date[2] . "/" . $date[0] . "/" . $date[1];
            if ($key == 0) {
                $daterendivo = $date;
            } else if ($key == 1) {
                $daterendivo1 = $date;

            } else if ($key == 2) {
                $daterendivo2 = $date;
            }
        }
//
//        echo $request->priode . '</br>';
//        echo $request->priode1 . '</br>';
//        echo $request->priode2 . '</br>';
//        dd($request->priode2);
        $transatExtra = ExtraTransat::insert([
            'daterendivo' => $daterendivo,
            'daterendivo1' => $daterendivo1,
            'daterendivo2' => $daterendivo2,
            'priode' => $request->priode,
            'priode1' => $request->priode1,
            'priode2' => $request->priode2,
            'customer_id' => $customer->id,
            'created_at' => Carbon::now()
        ]);

        $cronJob = new CronJob();
        $cronJob->customer_id = $customer->id;
        $cronJob->package_isp = 'Transat';
        $cronJob->save();

        if ($lang == 'en') {
            return redirect('http://innomobile.ca/en/promotion-thank-you-a/');
        } else if ($lang == 'fr') {
            return redirect('http://innomobile.ca/fr/promotion-merci-a/');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
