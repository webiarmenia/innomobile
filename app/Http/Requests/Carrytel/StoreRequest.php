<?php

namespace App\Http\Requests\Carrytel;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'package_of_interest' => 'required|string|exists:packages,package_id',
            'email' => 'required|email|unique:customers',
            'title' => 'required|string|max:4',
            'first_name' => 'required|string|max:30',
            'last_name' => 'required|string|max:30',
            'appartment_number' => ['nullable','regex:/^[A-Z0-9]*((-|\s)*[A-Z0-9])*$/'],
            'street_number' => 'required',
            'street_name' => 'required|string',
            'city' => 'required|string',
            'region_name' => 'required',
            'postal_code' => ['required', 'postal_code' => 'postal_code:CA', 'string'],
            'phone_number' =>  ['required', 'regex:/^[+0-9]*((-|\s)*[+0-9])*$/'],
        ];
    }
}
