<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'email',
        'language',
        'title',
        'first_name',
        'last_name',
        'appartment_number',
        'street_number',
        'street_name',
        'city',
        'region_name',
        'postal_code',
        'phone_number',
        'package_of_interest',
        'package_status',
        'dsl_eligible',
        'token'
    ];

    public function package()
    {
        return $this->hasOne(Package::class,'package_id','package_of_interest');
    }

    public function cron()
    {
        return $this->hasOne(CronJob::class,'customer_id','id');
    }

    public function Ciktel()
    {
        return $this->hasOne(ExtraCiktel::class,'customer_id','id');
    }

    public function Transat()
    {
        return $this->hasOne(ExtraTransat::class,'customer_id','id');
    }



}
