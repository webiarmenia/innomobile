<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CronJob;
use Carbon\Carbon;
use App\Package;
use App\ExtraTransat;
use HeadlessChromium\BrowserFactory;
use Exception;

class OrderRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All orders successfully created';
    protected $a = 0;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function confirmationEmail($package, $customer, $dateAprox)
    {
        $innomobileEmail = env('MAIL_USERNAME');
        $innomobileEmail2 = 'order@innomobile.ca';

        \Mail::send('emails.sendCompletedEmail', ['package' => $package, 'customer' => $customer, 'dateAprox' => $dateAprox], function ($message) use ($innomobileEmail, $innomobileEmail2) {
            $message->to($innomobileEmail2)->subject('New order');
            $message->from($innomobileEmail, 'Innomobile');
        });
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cronJobs = CronJob::where("status", false)->where("worked_time", "<", 2)->with('customer')->get();

        $log = json_encode($cronJobs) . date('Y-m-d H:s:i');
        file_put_contents('/var/www/innomobile/log_' . date("n.Y") . '.log', $log, FILE_APPEND);

        if (sizeof($cronJobs) > 0) {
            foreach ($cronJobs as $key => $cronJob) {
                try {
                    $dateAprox = [];
                    $id = $cronJob->customer_id;
                    $package = Package::where('package_id', $cronJob->customer->package_of_interest)->firstOrfail();

                    $factory = new BrowserFactory('chromium-browser');
                    $browser = $factory->createBrowser([
                        'windowSize' => [1920, 2000],
                        'sendSyncDefaultTimeout' => 15000,
                        'noSandbox' => true
                    ]);

                    $page = $browser->createPage();
                    if (!\File::isDirectory(public_path('images'))) {
                        \File::makeDirectory(public_path('images'));
                    }

                    if ($cronJob->package_isp === 'Transat') {
                        $extraTransat = ExtraTransat::where("customer_id", $id)->firstOrfail();
                        if($cronJob->customer->language == 'fr') {
                            $priodes = [2 => 'Matin (7h30 - midi)', 3 => 'Apres-midi (midi - 17h)', 4 => 'Soir (17h - 21h)', 5 => 'Jour entier (7h30 - 21h)'];
                        } else {
                            $priodes = [2 => 'Morning (7:30 AM - noon)', 3 => 'Afternoon (noon to 5:00 PM)', 4 => 'Evening (5:00 PM to 9:00 PM)'];
                        }

                        $dateAprox = ["Date approximative" => $extraTransat->daterendivo,
                                      "Période d'installation souhaitée" => $priodes[$extraTransat->priode],
                                      "Date approximative (1)" => $extraTransat->daterendivo1,
                                      "Période d'installation souhaitée (1)" => $priodes[$extraTransat->priode1],
                                      "Date approximative (2)" => $extraTransat->daterendivo1,
                                      "Période d'installation souhaitée (2)" => $priodes[$extraTransat->priode2] ];
                        $page->navigate('http://50.21.165.241:3323/transatVente_newV2/View/login.php')->waitForNavigation();
                        $evaluation = $page->evaluate(
                            '(() => {
                                document.querySelector(".form-control[name=\'user\']").value = "' . $package->login . '";
                                document.querySelector(".form-control[name=\'pass\']").value = "' . $package->password . '";
                                document.querySelector("form").submit();
                            })()'
                        );
                        $evaluation->waitForPageReload();
                        $page->screenshot()->saveToFile(public_path('images/Transat_New_1'. $cronJob . '-' . $key . '.png'));
                        $evaluation = $page->evaluate(
                            '(() => {
                                document.querySelector("a[href=\'AddCommande.php\']").click();
                            })()'
                        );
                        $evaluation->waitForPageReload();
                        $page->screenshot()->saveToFile(public_path('images/Transat_New_2'. $cronJob . '-' . $key . '.png'));


                        $address = $cronJob->customer->street_number . ',' . $cronJob->customer->street_name . ',' . $cronJob->customer->appartment_number;

                        $evaluation = $page->evaluate(
                            '(() => {
                            document.getElementById("forfait_id").value = "' . $package->back_id . '";
                            document.querySelector("input[name=\'nom\']").value = "' . $cronJob->customer->first_name . '";
                            document.querySelector("input[name=\'prenom\']").value = "' . $cronJob->customer->last_name . '";
                            document.querySelector("input[name=\'adresse\']").value = "' . $address . '";
                            document.querySelector("input[name=\'email\']").value = "' . $cronJob->customer->email . '";
                            document.querySelector("input[name=\'codePostal\']").value = "' . $cronJob->customer->postal_code . '";
                            document.querySelector("input[name=\'telephone\']").value = "' . $cronJob->customer->phone_number . '";
                            document.querySelector("input[name=\'Daterendivo\']").value = "' . $extraTransat->daterendivo . '";
                            document.querySelector("input[name=\'Daterendivo1\']").value = "' . $extraTransat->daterendivo1 . '";
                            document.querySelector("input[name=\'Daterendivo2\']").value = "' . $extraTransat->daterendivo2 . '";
                            document.querySelector("form").submit();
                        })()'
                        );

                        $evaluation->waitForPageReload();
                        $page->screenshot()->saveToFile(public_path('images/Transat_New_3'. $cronJob . '-' . $key . '.png'));
                        $browser->close();
                    } else if ($cronJob->package_isp === 'Carrytel') {
                        $page->navigate('https://www.carrytel.ca/agent/login.aspx')->waitForNavigation();
                        $evaluation = $page->evaluate(
                            '(() => {
                        document.querySelector(".logoinp[name=\'Username\']").value = "' . $package->login . '";
                        document.querySelector(".logoinp[name=\'Password\']").value = "' . $package->password . '";
                        document.querySelector(".btn1").click();
                    })()'
                        );
                        $evaluation->waitForPageReload();
                        $page->screenshot()->saveToFile(public_path('images/Carrytel_New_1.png'));
                        $evaluation = $page->evaluate(
                            '(() => {
                            location.replace("https://www.carrytel.ca/tools/add_order_user_iframe.aspx?plan='. $package->back_id .'&source=agent");
                        })()'
                        );
                        $evaluation->waitForPageReload();
                        $page->screenshot()->saveToFile(public_path('images/Carrytel_New_2.png'));

                        $phone = explode("-", $cronJob->customer->phone_number);
                        $cellphone_area = $phone[1];
                        $cellphone = $phone[2];
                        $newDate = Carbon::now()->addDays(5);

                        $newDate = $newDate->toDateString();
                        $evaluation = $page->evaluate(
                            '(() => {
                        document.getElementById("language").value = "' . $cronJob->customer->language . '";
                        document.querySelector("input[name=\'streetnumber\']").value = "' . $cronJob->customer->street_number . '";
                        document.querySelector("input[name=\'streetname\']").value = "' . $cronJob->customer->street_name . '";
                        document.querySelector("input[name=\'city\']").value = "' . $cronJob->customer->city . '";
                        document.querySelector("input[name=\'postcode\']").value = "' . $cronJob->customer->postal_code . '";
                        document.querySelector("#title").value = "' . $cronJob->customer->title . '";
                        document.querySelector("input[name=\'firstname\']").value = "' . $cronJob->customer->first_name . '";
                        document.querySelector("input[name=\'lastname\']").value = "' . $cronJob->customer->last_name . '";
                        document.querySelector("input[name=\'cellphone_area\']").value = "' . $cellphone_area . '";
                        document.querySelector("input[name=\'cellphone\']").value = "' . $cellphone . '";
                        document.querySelector("input[name=\'email\']").value = "' . $cronJob->customer->email . '";
                        document.querySelector("input[name=\'birth\']").value = "Unknown";
                        document.querySelector("input[name=\'photoidname\']").value = "' . $cronJob->customer->package_of_interest . '";
                        document.querySelector("input[name=\'date\']").value = "' . $newDate . '";
                        document.querySelector(".btn1").click();
                    })()'
                        );
                        $evaluation->waitForPageReload();
                        $this->a = $this->a + 1;
                        try {
                            $value = $page->evaluate('document.querySelector("select[name=\'language\'").value')->getReturnValue();
                            $page->screenshot()->saveToFile(public_path('images/Carrytel_New_3' . $cronJobs->id . '.png'));
                            throw new Exception("We have a error about backend validation ...");
                        } catch (\Exception $e) {
                            $page->screenshot()->saveToFile(public_path('images/Carrytel_New_3.png'));
                        }
                        $browser->close();
                    }
                    $cronJobs[$key]->status = true;
                    $cronJobs[$key]->worked_time = $cronJobs[$key]->worked_time + 1;
                    $cronJob->save();
                    $this->confirmationEmail($package, $cronJob->customer, $dateAprox);
                } catch (\Exception $e) {
                    $browser->close();
                    $cronJobs[$key]->worked_time = $cronJobs[$key]->worked_time + 1;
                    $cronJob->save();
                    $this->handle();
                }
            }
        }
    }

}
