<?php $lang = Config::get('app.locale'); ?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MXTXPPV');</script>
    <!-- End Google Tag Manager -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" id="redux-google-fonts-salient_redux-css"
          href="https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C600%2C800%2C400italic%7CPlayfair+Display%7CRoboto%3A300italic%2C400%2C700&amp;subset=latin&amp;ver=1553883325"
          type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/style.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('/css/responsive-style.css')}}"/>

    @yield('css')
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MXTXPPV"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<header>
    <div class="header">
        <a class="logo" href="https://innomobile.ca/en">
            <img src="{{ asset('images/Innomobile-Logo_white-2-01.svg') }} " alt="Innomobile_logo">
        </a>
        <div class="header-menu-in">
            <button class="dropbtn">
                <a class="menu-show">
                    <ul>
                        <li style="border:1px solid #fff; width: 22px; margin-bottom: 4px; margin-top: 4px;"></li>
                        <li style="border:1px solid #fff; width: 22px; margin-bottom: 4px; margin-top: 4px;"></li>
                        <li style="border:1px solid #fff; width: 22px; margin-bottom: 4px; margin-top: 4px;"></li>
                    </ul>
                </a>
            </button>
            <div class="header-right">
                <div class="no-social">
                    <ul class="menu-item-innomobile">
                        <li>
                            <a href="@if ($lang == 'en') https://innomobile.ca/en/internet-promotions
                                    @elseif ($lang == 'fr') https://innomobile.ca/fr/promotions-internet @endif"
                               class="promotion">{{ __('text.promotion') }}</a>
                        </li>
                        <li><a href="https://innomobile.ca/en/#Advantages">{{ __('text.advantages') }}</a></li>
                        <li class="have-sub-menu">
                            <a href="https://innomobile.ca/en/#Services">{{ __('text.services') }}
                                <span class="sub-menu-down sf-sub-indicator" style="height: 21px;">
                                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                </span>
                            </a>
                            <span class="sf-sub-indicator responsive-sub-menu-down">
                                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                </span>
                            <ul class="sub-menu-item">
                                <li>
                                    <a href="http://innomobile.ca/en/services/#Internet">
                                        <i class="fa fa-laptop" aria-hidden="true"></i>{{ __('text.internet') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="http://innomobile.ca/en/services/#HomePhone">
                                        <i class="fa fa-phone" aria-hidden="true"></i>{{ __('text.home_phone') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="http://innomobile.ca/en/services/#Television">
                                        <i class="fa fa-television" aria-hidden="true"></i>{{ __('text.television') }}
                                    </a>
                                </li>
                                <li>
                                    <a href="http://innomobile.ca/en/services/#Mobile">
                                        <i class="fa fa-mobile" aria-hidden="true"
                                           style="font-size: 19px;"></i>{{ __('text.mobile') }}
                                    </a>
                                </li>
                            </ul>
                            <div class="clear-fix"></div>
                        </li>
                        <li class="wi-li"><a
                                    href="https://innomobile.ca/en/questionnaire-en/">{{ __('text.questionnaire') }}</a>
                        </li>
                        <li><a href="https://innomobile.ca/en/about-us/">{{ __('text.about_us') }}</a></li>
                        <li><a href="https://innomobile.ca/en/contact/">{{ __('text.contact') }}</a></li>
                        <li>
                            <a href="@if ($lang == 'en') {{ url('locale\fr') }} @elseif ($lang == 'fr') {{ url('locale\en') }} @endif">@if ($lang == 'en')
                                    [FR] @elseif ($lang == 'fr') [EN] @endif</a></li>
                        <li class="header-li-social"><a style="margin-right: 0; margin-left: 30px;"
                                                        class="social-header" target="_blank"
                                                        href="https://www.facebook.com/FOLLOWINNOMOBILE/">
                                <i class="header-icon-facebook fa fa-facebook"></i>
                            </a></li>
                        <li class="header-li-social"><a style="margin-left: 0; margin-right: 23px;"
                                                        class="social-header" target="_blank"
                                                        href="https://www.instagram.com/followinnomobile/">
                                <i class="header-icon-instagram fa fa-instagram" aria-hidden="true"></i>
                            </a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="responsive-social-link-two">
                    <ul>
                        <li class="active-header-li-social header-li-social"><a
                                    style="margin-right: 0; margin-left: 30px;"
                                    class="social-header" target="_blank"
                                    href="https://www.facebook.com/FOLLOWINNOMOBILE/">
                                <i class="header-icon-facebook fa fa-facebook"></i>
                            </a></li>
                        <li class="active-header-li-social header-li-social"><a
                                    style="margin-left: 0; margin-right: 23px;"
                                    class="social-header" target="_blank"
                                    href="https://www.instagram.com/followinnomobile/">
                                <i class="header-icon-instagram fa fa-instagram" aria-hidden="true"></i>
                            </a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="header-social-responsive">
            <ul>
                <li>
                    <a class="social-header" target="_blank"
                       href="https://www.facebook.com/FOLLOWINNOMOBILE/">
                        <i class="header-icon-facebook fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a class="social-header" target="_blank"
                       href="https://www.instagram.com/followinnomobile/">
                        <i class="header-icon-instagram fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </li>
                <div class="clearfix"></div>
            </ul>
        </div>
    </div>
</header>
<section>

    <div class="images row" style="margin: 0;">
        <div class="images-two">
            <div class="transat-transition-left col-md-6 section-image-one">
                <div>
                    @if($package_type == 'dsl')
                        <img src="@if($lang == 'en'){{ asset('images/Fiber_Cable_HD-With-Header-HalfScreen-960x604.png') }}@elseif($lang == 'fr') {{ asset('images/Fiber_Cable_HD-With-Header-HalfScreen-960x604-FRENCH.png')}}@endif"
                             alt="fault1" class="section-img">
                    @elseif($package_type == 'cable')
                        <img src="@if($lang == 'en'){{ asset('images/CABLE-Internet-HalfScreen-Top-Left-ENGLISH.png') }}@elseif($lang == 'fr') {{ asset('images/CABLE-Internet-HalfScreen-Top-Left-FRENCH.png')}}@endif"
                             alt="fault1" class="section-img">
                    @endif
                </div>
            </div>
            <div class="transat-transition-right col-md-6 section-image-two">
                <div style="line-height: 1;">
                    <div>
                        @if($isp === 'Transat')
                            @if($package_type === 'dsl')
                                <img src="@if($lang == 'en'){{ asset('images/Pricing-Table-Transat-Fibre-English.png') }}@elseif($lang == 'fr') {{ asset('images/Pricing-Table-Transat-Fibre-French.png') }}@endif"
                                     alt="fault2" class="section-img" width="1200" height="771"
                                     style="max-width: 100%; height: auto;">
                            @elseif($package_type === 'cable')
                                <img src="@if($lang == 'en'){{ asset('images/Pricing-Table-Transat-Cable-English.png') }}@elseif($lang == 'fr') {{ asset('images/Pricing-Table-Transat-Cable-French.png') }}@endif"
                                     alt="fault2" class="section-img" width="1200" height="771"
                                     style="max-width: 100%; height: auto;">
                            @endif
                        @elseif($isp === 'Virgin')
                            @if($package_type === 'dsl')
                                <img src="@if($lang == 'en'){{ asset('images/Pricing-Table-Virgin-Fibre-English.png') }}@elseif($lang == 'fr') {{ asset('images/Pricing-Table-Virgin-Fibre-French.png') }}@endif"
                                     alt="fault2" class="section-img" width="1200" height="771"
                                     style="max-width: 100%; height: auto;">
                            @endif
                        @elseif($isp === 'Carrytel')
                            @if($package_type === 'dsl')
                                <img src="@if($lang == 'en'){{ asset('images/Pricing-Table-Carrytel-Fibre-English.png') }}@elseif($lang == 'fr') {{ asset('images/Pricing-Table-Carrytel-Fibre-French.png') }}@endif"
                                     alt="fault2" class="section-img" width="1200" height="771"
                                     style="max-width: 100%; height: auto;">
                            @elseif($package_type === 'cable')
                                <img src="@if($lang == 'en'){{ asset('images/Pricing-Table-Carrytel-Cable-English.png') }}@elseif($lang == 'fr') {{ asset('images/Pricing-Table-Carrytel-Cable-French.png') }}@endif"
                                     alt="fault2" class="section-img" width="1200" height="771"
                                     style="max-width: 100%; height: auto;">
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('content')
</section>
<footer style="background: #00549a; padding: 30px 0 70px;">
    <div class="container">
        <div>
            <ul style="width: 144px; margin: auto; padding-left: 0;">
                <li style="float: left; display:inline-block; margin: 0 10px 25px 10px;">
                    <a class="link" target="_blank"
                       href="https://www.facebook.com/FOLLOWINNOMOBILE/" style="padding: 0; position: relative;"><i
                                class="footer-icon fa fa-facebook"></i> </a>
                </li>
                <li style="display:inline-block; margin: 0 10px 25px 10px;">
                    <a class="link" target="_blank"
                       href="https://www.instagram.com/followinnomobile/" style="padding: 0; position: relative;"><i
                                class="footer-icon fa fa-instagram"></i></a>
                </li>
            </ul>
        </div>
        <div class="row">
            <p style="font-family: 'Open Sans'; color:white; text-align: center; font-size: 12px;">© 2020
                Innomobile. {{ __('text.copy_right') }}</p>
        </div>
    </div>
</footer>

@yield('javascript')
<script src="{{ asset('/js/maskedPhone.js') }}"></script>
<script src="{{ asset('/js/js.js') }}"></script>
<script src="{{ asset('/js/responsive-js.js') }}"></script>
</body>
</html>
