@extends('layouts.email')

@section('content')
    <p>Customer ID - {{ $customer->id }}</p>
    <p>Email - {{ $customer->email }}</p>
    <p>Language - {{ $customer->language }}</p>
    <p>Title - {{ $customer->title }}</p>
    <p>First Name - {{ $customer->first_name }}</p>
    <p>Last Name - {{ $customer->last_name }}</p>
    <p>Appartment number - {{ $customer->appartment_number }}</p>
    <p>Street number - {{ $customer->street_number }}</p>
    <p>Street name - {{ $customer->street_name }}</p>
    <p>City - {{ $customer->city }}</p>
    <p>Province - {{ $customer->region_name }}</p>
    <p>Postal code - {{ $customer->postal_code }}</p>
    <p>Phone number - {{ $customer->phone_number }}</p>
    <p>Package of Interest - {{ $customer->package_of_interest }}</p>
    <p>Package Status - {{ $customer->package_status }}</p>
    <p>Eligible for DSL - {{ $customer->dsl_eligible }}</p>
    <p>Created At - {{ $customer->created_at }}</p>
    @foreach($dateAprox as $key => $value)
        <p>{{ $key }} - {{ $value }}</p>
    @endforeach

@endsection
