@extends('layouts.email')

@section('content')
    <div>
        <img src="{{ $message->embed(public_path() . '/images/Innomobile-PNG-Customer-Email-Header-2x-LEFT.png') }}"
             alt="Innomobile" width="600" height="150">

        <p>Dear Virgin Agent, the following Virgin DSL Internet Service order has been requested from a customer of
            Innomobile.</p>
        <p>Please follow-up with the customer to complete the order.</p>

        <div style="padding-left: 30px;">
            <p>Package name: {{ $package->package_name }}</p>
            <p>Package price: {{ $package->package_price }}{{ $package->currency }}</p>
            <p>First name: {{ $customer->first_name }}</p>
            <p>Last name: {{ $customer->last_name }}</p>
            <p>Email address: {{ $customer->email }}</p>
            <p>Apartment number: {{ $customer->appartment_number }}</p>
            <p>Street number: {{ $customer->street_number }}</p>
            <p>Street name: {{ $customer->street_name }}</p>
            <p>City name: {{ $customer->city }}</p>
            <p>Region name: {{ $customer->region_name }}</p>
            <p>Postal code: {{ $customer->postal_code }}</p>
            <p>Phone number: {{ $customer->phone_number }}</p>
        </div>

        <p>
            <span>Thank you,</span></br>
            <span>The Innomobile Team</span></br>
            <span>Shawn José Lucien</span></br>
            <span>shawn@innomobile.ca</span></br>
            <span>514-692-6864</span></br>
        </p>
    </div>

@endsection
