@extends('layouts.layouts', [
    'package' => 'a',
    'package_type' => $package_type
])
@section('title')
    @if (Config::get('app.locale') == 'en'){{ $remove_select_heading }}
        Promotion @elseif (Config::get('app.locale') == 'fr') Promotion {{ $remove_select_heading }} @endif
@endsection

@section('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <a class="to-form-transat" href="#transat-form-id" style="display: none;"></a>
    <div class="container" id="transat-form-id">
        <div class="row">
            <div class="col-md-6">
                <div class="titles-before-form">
                    <p>
                        <strong>
                            <span>{{ __('text.before_form_h2') }}</span>
                        </strong>
                    </p>
                    <p>
                        @if($package_type == 'dsl')
                            {{ __('text.before_form_h4_dsl') }}
                        @elseif($package_type == 'cable')
                            {{ __('text.before_form_h4_cable') }}
                        @endif
                    </p>
                </div>

                <div id="table" class="form-div table-editable">

                    <form method="post" action="{{url(''.$lang. '/a/' . $package_type . '/' .'customer/store')}}"
                          enctype="multipart/form-data" autocomplete="off">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="package_of_interest" class="form-control">
                                    @if($lang == 'en')
                                        @foreach($packages as $package)
                                            <option value="{{$package['package_id']}}"
                                            @if($package_type == 'dsl')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @elseif($package_type == 'cable')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @endif>
                                                @if($package_type == 'dsl')
                                                    {{ str_replace($remove_select_heading,__('text.fiber') ,$package['package_name']) }}
                                                @elseif($package_type == 'cable')
                                                    {{ $package['package_name'] }}
                                                @endif
                                                {{ $package['currency'].$package['package_price'] }}
                                            </option>
                                        @endforeach
                                    @elseif($lang = 'fr')
                                        @foreach($packages as $package)
                                            <option value="{{$package['package_id']}}"
                                            @if($package_type == 'dsl')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @elseif($package_type == 'cable')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @endif>
                                                @if($package_type == 'dsl')
                                                    {{ str_replace($remove_select_heading,__('text.fiber') ,$package['package_name']) }}
                                                @elseif($package_type == 'cable')
                                                    {{ $package['package_name'] }}
                                                @endif
                                                {{ str_replace('.', ',', $package['package_price']).$package['currency'] }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('package_of_interest'))
                                    <span class="error inn-error">{{ $errors->first('package_of_interest') }}*</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="email" name="email"
                                       value="{{ old('email') }}" placeholder="{{ __('text.your_email') }}*">
                                @if ($errors->has('email'))
                                    <span class="error inn-error">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="title"
                                        value="{{ old('title') }}">
                                    <option disabled selected="selected">{{ __('text.title') }}*</option>
                                    <option value="Mr." {{ old('title') == "Mr." ? 'selected' : '' }}>Mr.</option>
                                    <option value="Mrs." {{ old('title') == "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                    <option value="Miss" {{ old('title') == "Miss." ? 'selected' : '' }}>Miss</option>
                                    <option value="Ms." {{ old('title') == "Ms." ? 'selected' : '' }}>Ms.</option>
                                </select>
                                @if ($errors->has('title'))
                                    <span class="error inn-error">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="first_name"
                                       value="{{ old('first_name') }}" placeholder="{{ __('text.your_first_name') }}*">
                                @if ($errors->has('first_name'))
                                    <span class="error inn-error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="last_name"
                                       value="{{ old('last_name') }}" placeholder="{{ __('text.your_last_name') }}*">
                                @if ($errors->has('last_name'))
                                    <span class="error inn-error">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <label class="appartment-number-label control-label">{{ __('text.appartment_number_label') }}</label>
                                <input class="input col-md-12" type="text" name="appartment_number"
                                       value="{{ old('appartment_number') }}"
                                       placeholder="{{ __('text.your_appartment_number') }}">
                                @if ($errors->has('appartment_number'))
                                    <span class="error inn-error">{{ $errors->first('appartment_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="street_number"
                                       value="{{ old('street_number') }}"
                                       placeholder="{{ __('text.your_street_number') }}*">
                                @if ($errors->has('street_number'))
                                    <span class="error inn-error">{{ $errors->first('street_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="street_name"
                                       value="{{ old('street_name') }}"
                                       placeholder="{{ __('text.your_street_name') }}*">
                                @if ($errors->has('street_name'))
                                    <span class="error inn-error">{{ $errors->first('street_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="city"
                                       value="{{ old('city') }}" placeholder="{{ __('text.your_city') }}*">
                                @if ($errors->has('city'))
                                    <span class="error inn-error">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="postal_code"
                                       value="{{ old('postal_code') }}"
                                       placeholder="{{ __('text.your_postal_code') }}*">
                                @if ($errors->has('postal_code'))
                                    <span class="error inn-error">{{ $errors->first('postal_code') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input id="phone" type="text" class="transat col-md-12 input" name="phone_number"
                                       placeholder="{{ __('text.your_phone_number') }}*"
                                       value="{{ old('phone_number') }}">
                                @if ($errors->has('phone_number'))
                                    <span class="error inn-error">{{ $errors->first('phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <!-- Date -->
                            <div class="col-md-12 input-div">
                                <input type="text" name="daterendivo" class="col-md-12 input"
                                       id="datepicker1" placeholder="{{ __('text.approximate_date') }} (1) (*)"
                                       value="{{ old('daterendivo') }}">
                                @if ($errors->has('daterendivo'))
                                    <span class="error inn-error">{{ $errors->first('daterendivo') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="priode" class="form-control"
                                        value="{{ old('priode') }}">
                                    <option disabled selected="selected">{{ __('text.desired_installation_period') }}
                                        (1) (*)
                                    </option>
                                    @if(Config::get('app.locale') == 'en')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Morning (7:30 AM
                                            - noon)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Afternoon (noon
                                            to 5:00 PM)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Evening (5:00 PM
                                            to 9:00 PM)
                                        </option>
                                    @elseif(Config::get('app.locale') == 'fr')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Matin (7h30 -
                                            midi)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Apres-midi (midi
                                            - 17h)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Soir (17h -
                                            21h)
                                        </option>
                                        <option value="5" {{ old('priode') == "5" ? 'selected' : '' }}>Jour entier (7h30
                                            - 21h)
                                        </option>
                                    @endif
                                </select>
                                @if ($errors->has('priode'))
                                    <span class="error inn-error">{{ $errors->first('priode') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input type="text" name="daterendivo1" class="col-md-12 input"
                                       id="datepicker2" placeholder="{{ __('text.approximate_date') }} (2) (*)"
                                       value="{{ old('daterendivo1') }}">
                                @if ($errors->has('daterendivo1'))
                                    <span class="error inn-error">{{ $errors->first('daterendivo1') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="priode1" class="form-control"
                                        value="{{ old('priode1') }}">
                                    <option disabled selected="selected">{{ __('text.desired_installation_period') }}
                                        (2) (*)
                                    </option>

                                    @if(Config::get('app.locale') == 'en')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Morning (7:30 AM
                                            - noon)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Afternoon (noon
                                            to 5:00 PM)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Evening (5:00 PM
                                            to 9:00 PM)
                                        </option>
                                    @elseif(Config::get('app.locale') == 'fr')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Matin (7h30 -
                                            midi)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Apres-midi (midi
                                            - 17h)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Soir (17h -
                                            21h)
                                        </option>
                                        <option value="5" {{ old('priode') == "5" ? 'selected' : '' }}>Jour entier (7h30
                                            - 21h)
                                        </option>
                                    @endif
                                </select>
                                @if ($errors->has('priode1'))
                                    <span class="error inn-error">{{ $errors->first('priode1') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input type="text" name="daterendivo2" class="col-md-12 input"
                                       id="datepicker3" placeholder="{{ __('text.approximate_date') }} (3) (*)"
                                       value="{{ old('daterendivo2') }}" min="<?php echo date('2020-01-15'); ?>">
                                @if ($errors->has('daterendivo2'))
                                    <span class="error inn-error">{{ $errors->first('daterendivo2') }}</span>
                                @endif
                            </div>
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="priode2" class="form-control"
                                        value="{{ old('priode2') }}">
                                    <option disabled selected="selected">{{ __('text.desired_installation_period') }}
                                        (3) (*)
                                    </option>
                                    @if(Config::get('app.locale') == 'en')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Morning (7:30 AM
                                            - noon)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Afternoon (noon
                                            to 5:00 PM)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Evening (5:00 PM
                                            to 9:00 PM)
                                        </option>
                                    @elseif(Config::get('app.locale') == 'fr')
                                        <option value="2" {{ old('priode') == "2" ? 'selected' : '' }}>Matin (7h30 -
                                            midi)
                                        </option>
                                        <option value="3" {{ old('priode') == "3" ? 'selected' : '' }}>Apres-midi (midi
                                            - 17h)
                                        </option>
                                        <option value="4" {{ old('priode') == "4" ? 'selected' : '' }}>Soir (17h -
                                            21h)
                                        </option>
                                        <option value="5" {{ old('priode') == "5" ? 'selected' : '' }}>Jour entier (7h30
                                            - 21h)
                                        </option>
                                    @endif
                                </select>
                                @if ($errors->has('priode2'))
                                    <span class="error inn-error">{{ $errors->first('priode2') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 quebec-style">
                                <p>{{ __('text.before_send_firt_p') }}</p>
                                <p>{{ __('text.before_send_second_p') }}</p>
                                <span class="quebec-checkbox">
                                    <input type="checkbox" name="region_name">
                                    <span>{{ __('text.before_send_span') }}</span>
                                </span>
                                @if ($errors->has('region_name'))
                                    <span class="error inn-error">This field is required.</span>
                                @endif
                            </div>
                        </div>
                        <div class="row div-send-button">
                            <div class="col-md-12 input-div">
                                <input type="submit" class="btn btn-primary" value="{{ __('text.send') }}"
                                       style="padding: 15px 22px; line-height: 14px;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $package_type = '{{$package_type}}';
        $(function () {
            if ($package_type == 'dsl') {
                $("#datepicker1").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 2,
                });
                $("#datepicker2").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 2
                });
                $("#datepicker3").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 2
                });
            } else if ($package_type == 'cable') {
                $("#datepicker1").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 7
                });
                $("#datepicker2").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 7
                });
                $("#datepicker3").datepicker({
                    beforeShowDay:
                        function (dt) {
                            return [dt.getDay() == 6 || dt.getDay() == 0 ? false : true];
                        },
                    minDate: 7
                });
            }
        });
    </script>
    @if ($errors->any())
        <script>
            setTimeout(function () {
                document.querySelector(".to-form-transat").click();
            }, 200);
        </script>
    @endif
@endsection
