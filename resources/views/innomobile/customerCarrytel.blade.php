@extends('layouts.layouts', [
    'package' => 'b',
    'package_type' => $package_type
])
@section('title')
    @if (Config::get('app.locale') == 'en'){{ $remove_select_heading }}
        Promotion @elseif (Config::get('app.locale') == 'fr') Promotion {{ $remove_select_heading }} @endif
@endsection

@section('css')
@endsection

@section('content')
    <a class="to-form-carrytel" href="#carrytel-form-id" style="display: none;"></a>
    <div class="container" id="carrytel-form-id">
        <div class="row">
            <div class="col-md-6">
                <div class="titles-before-form">
                    <p>
                        <strong>
                            <span>{{ __('text.before_form_h2') }}</span>
                        </strong>
                    </p>
                    <p>
                        @if($package_type == 'dsl')
                            {{ __('text.before_form_h4_dsl') }}
                        @elseif($package_type == 'cable')
                            {{ __('text.before_form_h4_cable') }}
                        @endif
                    </p>
                </div>

                <div id="table" class="form-div table-editable">

                    <form method="post" action="{{url(''.$lang. '/b/' . $package_type . '/customer/store')}}"
                          enctype="multipart/form-data" autocomplete="off">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 select-field">
                                <label for="carrytel-package"></label>
                                <select class="select-div col-md-12" name="package_of_interest" class="form-control" id="carrytel-package">
                                    @if($lang == 'en')
                                        @foreach($packages as $package)
                                            <option value="{{$package['package_id']}}"
                                            @if($package_type == 'dsl')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @elseif($package_type == 'cable')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @endif>
                                                @if($package_type == 'dsl')
                                                    {{ str_replace($remove_select_heading,__('text.fiber') ,$package['package_name']) }}
                                                @elseif($package_type == 'cable')
                                                    {{ $package['package_name'] }}
                                                @endif
                                                {{ $package['currency'].$package['package_price'] }}
                                            </option>
                                        @endforeach
                                    @elseif($lang = 'fr')
                                        @foreach($packages as $package)
                                            <option value="{{$package['package_id']}}"
                                            @if($package_type == 'dsl')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @elseif($package_type == 'cable')
                                                {{ old('package_of_interest', $selectedPackageId) == $package['package_id'] ? 'selected' : '' }}
                                                    @endif>
                                                @if($package_type == 'dsl')
                                                    {{ str_replace($remove_select_heading,__('text.fiber') ,$package['package_name']) }}
                                                @elseif($package_type == 'cable')
                                                    {{ $package['package_name'] }}
                                                @endif
                                                {{ str_replace('.', ',', $package['package_price']).$package['currency'] }}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                                @if ($errors->has('package_of_interest'))
                                    <span class="error inn-error">{{ $errors->first('package_of_interest') }}*</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="email" name="email"
                                       value="{{ old('email') }}" placeholder="{{ __('text.your_email') }}*">
                                @if ($errors->has('email'))
                                    <span class="error inn-error">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row" style="display: none;">
                            <div class="col-md-12 select-field">
                                <input class="input col-md-12" type="text" name="language"
                                       value="{{ Config::get('app.locale') }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 select-field">
                                <select class="select-div col-md-12" name="title"
                                        value="{{ old('title') }}">
                                    <option disabled selected="selected">{{ __('text.title') }}*</option>
                                    <option value="Mr." {{ old('title') == "Mr." ? 'selected' : '' }}>Mr.</option>
                                    <option value="Mrs." {{ old('title') == "Mrs." ? 'selected' : '' }}>Mrs.</option>
                                    <option value="Miss" {{ old('title') == "Miss." ? 'selected' : '' }}>Miss</option>
                                    <option value="Ms." {{ old('title') == "Ms." ? 'selected' : '' }}>Ms.</option>
                                </select>
                                @if ($errors->has('title'))
                                    <span class="error inn-error">{{ $errors->first('title') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="first_name"
                                       value="{{ old('first_name') }}" placeholder="{{ __('text.your_first_name') }}*">
                                @if ($errors->has('first_name'))
                                    <span class="error inn-error">{{ $errors->first('first_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="last_name"
                                       value="{{ old('last_name') }}" placeholder="{{ __('text.your_last_name') }}*">
                                @if ($errors->has('last_name'))
                                    <span class="error inn-error">{{ $errors->first('last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="street_number"
                                       value="{{ old('street_number') }}"
                                       placeholder="{{ __('text.your_street_number') }}*">
                                @if ($errors->has('street_number'))
                                    <span class="error inn-error">{{ $errors->first('street_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="street_name"
                                       value="{{ old('street_name') }}"
                                       placeholder="{{ __('text.your_street_name') }}*">
                                @if ($errors->has('street_name'))
                                    <span class="error inn-error">{{ $errors->first('street_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12" type="text" name="city"
                                       value="{{ old('city') }}" placeholder="{{ __('text.your_city') }}*">
                                @if ($errors->has('city'))
                                    <span class="error inn-error">{{ $errors->first('city') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input class="input col-md-12 carrytel-postal-code" type="text" name="postal_code"
                                       value="{{ old('postal_code') }}"
                                       placeholder="{{ __('text.your_postal_code') }}*">
                                @if ($errors->has('postal_code'))
                                    <span class="error inn-error">{{ $errors->first('postal_code') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 input-div">
                                <input id="phone" type="text" class="carrytel col-md-12 input" name="phone_number"
                                       placeholder="{{ __('text.your_phone_number') }}*"
                                       value="{{ old('phone_number') }}">
                                @if ($errors->has('phone_number'))
                                    <span class="error inn-error">{{ $errors->first('phone_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 quebec-style">
                                <p>{{ __('text.before_send_firt_p') }}</p>
                                <p>{{ __('text.before_send_second_p') }}</p>
                                <span class="quebec-checkbox">
                                    <input type="checkbox" name="region_name">
                                    <span>{{ __('text.before_send_span') }}</span>
                                </span>
                                @if ($errors->has('region_name'))
                                    <span class="error inn-error">This field is required.</span>
                                @endif
                            </div>
                        </div>
                        <div class="row div-send-button">
                            <div class="col-md-12 input-div">
                                <input type="submit" class="btn btn-primary" value="{{ __('text.send') }}"
                                       style="padding: 15px 22px; line-height: 14px;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('javascript')
    @if ($errors->any())
        <script>
            setTimeout(function () {
                document.querySelector(".to-form-carrytel").click();
            }, 200);
        </script>
    @endif
@endsection

