@extends('layouts.app')

@section('title')
    <title>Customer update - {{ $customer->email }}</title>
@endsection('title')

@section('css')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4 wi-page-title"
            id="active-customer-page">Update customer</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('admin.customerData.update', $customer->id) }}"
                      enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label>Email <span class="asterisk">*</span> </label>
                        <input type="email" name="email" class="form-control"
                               value="{{ old( 'email', $customer->email) }}">
                    </div>

                    <div class="form-group">
                        <label>Language <span class="asterisk">*</span> </label>
                        <select name="language" class="form-control" value="{{ old('language') }}">
                            <option value="en" {{ old('language', $customer->language) == "en" ? 'selected' : '' }}>
                                English
                            </option>
                            <option value="fr" {{ old('language', $customer->language) == "fr" ? 'selected' : '' }}>
                                French
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title <span class="asterisk">*</span> </label>
                        <select name="title" class="form-control" value="{{ old('title') }}">
                            <option selected="selected"></option>
                            <option value="Mr." {{ old('title', $customer->title) == "Mr." ? 'selected' : '' }}>Mr.
                            </option>
                            <option value="Mrs." {{ old('title', $customer->title) == "Mrs." ? 'selected' : '' }}>Mrs.
                            </option>
                            <option value="Miss" {{ old('title', $customer->title) == "Miss" ? 'selected' : '' }}>Miss
                            </option>
                            <option value="Ms." {{ old('title', $customer->title) == "Ms." ? 'selected' : '' }}>Ms.
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>First name <span class="asterisk">*</span> </label>
                        <input type="text" name="first_name" class="form-control"
                               value="{{ old( 'first_name', $customer->first_name) }}">
                    </div>

                    <div class="form-group">
                        <label>Last name <span class="asterisk">*</span> </label>
                        <input type="text" name="last_name" class="form-control"
                               value="{{ old( 'last_name', $customer->last_name) }}">
                    </div>

                    <div class="form-group">
                        <label>Appartment number</label>
                        <input type="text" name="appartment_number" class="form-control"
                               value="{{ old( 'appartment_number', $customer->appartment_number) }}">
                    </div>

                    <div class="form-group">
                        <label>Street number <span class="asterisk">*</span> </label>
                        <input type="text" name="street_number" class="form-control"
                               value="{{ old( 'street_number', $customer->street_number) }}">
                    </div>

                    <div class="form-group">
                        <label>Street name <span class="asterisk">*</span> </label>
                        <input type="text" name="street_name" class="form-control"
                               value="{{ old( 'street_name', $customer->street_name) }}">
                    </div>

                    <div class="form-group">
                        <label>City <span class="asterisk">*</span> </label>
                        <input type="text" name="city" class="form-control" value="{{ old( 'city', $customer->city) }}">
                    </div>

                    <div class="form-group">
                        <label>Region name <span class="asterisk">*</span> </label>
                        <select name="region_name" class="form-control">
                            <option disabled="disabled" selected="selected">Region name</option>
                            <option value="{{ old( 'region_name', $customer->region_name) }}" selected="selected">
                                Quebec
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Postal code <span class="asterisk">*</span> </label>
                        <input type="text" name="postal_code" class="form-control"
                               value="{{ old( 'postal_code', $customer->postal_code) }}">
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone number <span class="asterisk">*</span> </label>
                        <input id="phone" type="text" class="form-control transat" name="phone_number"
                               placeholder="+1(000)-000-0000"
                               value="{{ old( 'phone_number', $customer->phone_number) }}">
                    </div>

                    <div class="form-group">
                        <label>Package of Interest <span class="asterisk">*</span> </label>
                        <select name="package_of_interest" class="form-control">
                            <option disabled="disabled" selected="selected">Package of Interest</option>
                            @foreach($packages as $package)
                                <option value="{{$package['package_id']}}" {{ old('package_of_interest', $customer->package_of_interest) == $package['package_id'] ? 'selected' : '' }}>{{$package['package_name'].' - '.$package['package_isp']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Package status</label>
                        <br>
                        <select name="package_status" class="form-control">
                            <option disabled="disabled" selected="selected">Package Status</option>
                            <option value="Desired" {{ old('package_status', 'Desired') == $customer->package_status ? 'selected' : '' }}>
                                Desired
                            </option>
                            <option value="Cable Offered as Alternative" {{ old('package_status', 'Cable Offered as Alternative') == $customer->package_status ? 'selected' : '' }}>
                                Cable Offered as Alternative
                            </option>
                            <option value="DSL Eligibility Confirmed" {{ old('package_status', 'DSL Eligibility Confirmed') == $customer->package_status ? 'selected' : '' }}>
                                DSL Eligibility Confirmed
                            </option>
                            <option value="Interested in Cable" {{ old('package_status', 'Interested in Cable') == $customer->package_status ? 'selected' : '' }}>
                                Interested in Cable
                            </option>
                            <option value="Ordered" {{ old('package_status', 'Ordered') == $customer->package_status ? 'selected' : '' }}>
                                Ordered
                            </option>
                        </select>
                        @if ($errors->has('package_status'))
                            <span class="error inn-error">{{ $errors->first('package_status') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Dsl eligible</label>
                        <br>
                        <select name="dsl_eligible" class="form-control">
                            <option value="Unknown" {{ old('dsl_eligible', 'Unknown') == $customer->dsl_eligible ? 'selected' : '' }}>
                                Dsl Eligible
                            </option>
                            <option value="yes" {{ old('dsl_eligible', 'yes') == $customer->dsl_eligible ? 'selected' : '' }}>
                                Yes
                            </option>
                            <option value="no" {{ old('dsl_eligible', 'no') == $customer->dsl_eligible ? 'selected' : '' }}>
                                No
                            </option>
                        </select>
                        @if ($errors->has('dsl_eligible'))
                            <span class="error inn-error">{{ $errors->first('dsl_eligible') }}</span>
                        @endif
                    </div>
                    @if($customer->Transat != null)

                        <div class="row">
                            <!-- Date -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date approximative (*) </label>

                                    <div class="input-group date" id="datetimepicker1">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="daterendivo" class="form-control pull-right"
                                               id="datepicker" value="{{ old( 'daterendivo', $arrayDateEdit[0]) }}"
                                               readonly>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Période d'installation souhaitée (*) </label>
                                    <select class="form-control select2 select2-hidden-accessible" name="priode"
                                            style="width: 100%;" tabindex="-1" aria-hidden="true" disabled>
                                        <option></option>
                                        <option value="2" {{ old('priode', $customer->Transat->priode) == 2 ? 'selected' : '' }}>
                                            Matin (7h30 - midi)
                                        </option>
                                        <option value="3" {{ old('priode', $customer->Transat->priode) == 3 ? 'selected' : '' }}>
                                            Apres-midi (midi - 17h)
                                        </option>
                                        <option value="4" {{ old('priode', $customer->Transat->priode) == 4 ? 'selected' : '' }}>
                                            Soir (17h - 21h)
                                        </option>
                                        <option value="5" {{ old('priode', $customer->Transat->priode) == 5 ? 'selected' : '' }}>
                                            Jour entier (7h30 - 21h)
                                        </option>
                                    </select><span class="select2 select2-container select2-container--default"
                                                   dir="ltr" style="width: 100%;"><span class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-labelledby="select2-Priode-8i-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-Priode-8i-container" title=""></span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Date -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date approximative (1) (*) </label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="daterendivo1" class="form-control pull-right"
                                               id="datepicker1" value="{{ old( 'daterendivo1', $arrayDateEdit[1]) }}"
                                               readonly>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Période d'installation souhaitée (1) (*) </label>
                                    <select class="form-control select2 select2-hidden-accessible" name="priode1"
                                            style="width: 100%;" tabindex="-1" aria-hidden="true" disabled>
                                        <option></option>
                                        <option value="2" {{ old('priode1', $customer->Transat->priode1) == 2 ? 'selected' : '' }}>
                                            Matin (7h30 - midi)
                                        </option>
                                        <option value="3" {{ old('priode1', $customer->Transat->priode1) == 3 ? 'selected' : '' }}>
                                            Apres-midi (midi - 17h)
                                        </option>
                                        <option value="4" {{ old('priode1', $customer->Transat->priode1) == 4 ? 'selected' : '' }}>
                                            Soir (17h - 21h)
                                        </option>
                                        <option value="5" {{ old('priode1', $customer->Transat->priode1) == 5 ? 'selected' : '' }}>
                                            Jour entier (7h30 - 21h)
                                        </option>
                                    </select><span class="select2 select2-container select2-container--default"
                                                   dir="ltr" style="width: 100%;"><span class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-labelledby="select2-Priode1-x0-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-Priode1-x0-container" title=""></span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- Date -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Date approximative (2) (*) </label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="daterendivo2" class="form-control pull-right"
                                               id="datepicker2" value="{{ old( 'daterendivo2', $arrayDateEdit[2])}}"
                                               readonly>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Période d'installation souhaitée (2) (*) </label>
                                    <select class="form-control select2 select2-hidden-accessible" name="Priode2"
                                            style="width: 100%;" tabindex="-1" aria-hidden="true" disabled>
                                        <option></option>
                                        <option value="2" {{ old('priode2', $customer->Transat->priode2) == 2 ? 'selected' : '' }}>
                                            Matin (7h30 - midi)
                                        </option>
                                        <option value="3" {{ old('priode2', $customer->Transat->priode2) == 3 ? 'selected' : '' }}>
                                            Apres-midi (midi - 17h)
                                        </option>
                                        <option value="4" {{ old('priode2', $customer->Transat->priode2) == 4 ? 'selected' : '' }}>
                                            Soir (17h - 21h)
                                        </option>
                                        <option value="5" {{ old('priode2', $customer->Transat->priode2) == 5 ? 'selected' : '' }}>
                                            Jour entier (7h30 - 21h)
                                        </option>
                                    </select><span class="select2 select2-container select2-container--default"
                                                   dir="ltr" style="width: 100%;"><span class="selection"><span
                                                    class="select2-selection select2-selection--single" role="combobox"
                                                    aria-haspopup="true" aria-expanded="false" tabindex="0"
                                                    aria-labelledby="select2-Priode2-89-container"><span
                                                        class="select2-selection__rendered"
                                                        id="select2-Priode2-89-container" title=""></span><span
                                                        class="select2-selection__arrow" role="presentation"><b
                                                            role="presentation"></b></span></span></span><span
                                                class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                            </div>
                        </div>

                    @endif

                    <div class="form-group">
                        <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection



