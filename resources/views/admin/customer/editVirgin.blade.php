@extends('layouts.app')

@section('title')
    <title>Customer update - {{ $customer->email }}</title>
@endsection('title')

@section('css')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('content')
    <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4 wi-page-title"
            id="active-customer-page">Update customer</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{ route('admin.customerData.update', $customer->id) }}"
                      enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label>Package of Interest <span class="asterisk">*</span> </label>
                        <select name="package_of_interest" class="form-control">
                            <option disabled="disabled" selected="selected">Package of Interest</option>
                            @foreach($packages as $package)
                                <option value="{{$package['package_id']}}" {{ old('package_of_interest', $customer->package_of_interest) == $package['package_id'] ? 'selected' : '' }}>{{$package['package_name'].' - '.$package['package_isp']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Email <span class="asterisk">*</span> </label>
                        <input type="email" name="email" class="form-control"
                               value="{{ old( 'email', $customer->email) }}">
                    </div>

                    <div class="form-group">
                        <label>Language <span class="asterisk">*</span> </label>
                        <select name="language" class="form-control" value="{{ old('language') }}">
                            <option value="en" {{ old('language', $customer->language) == "en" ? 'selected' : '' }}>
                                English
                            </option>
                            <option value="fr" {{ old('language', $customer->language) == "fr" ? 'selected' : '' }}>
                                French
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title <span class="asterisk">*</span> </label>
                        <select name="title" class="form-control" value="{{ old('title') }}">
                            <option value="Mr." {{ old('title', $customer->title) == "Mr." ? 'selected' : '' }}>Mr.
                            </option>
                            <option value="Mrs." {{ old('title', $customer->title) == "Mrs." ? 'selected' : '' }}>Mrs.
                            </option>
                            <option value="Miss" {{ old('title', $customer->title) == "Miss" ? 'selected' : '' }}>Miss
                            </option>
                            <option value="Ms." {{ old('title', $customer->title) == "Ms." ? 'selected' : '' }}>Ms.
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>First name <span class="asterisk">*</span> </label>
                        <input type="text" name="first_name" class="form-control"
                               value="{{ old( 'first_name', $customer->first_name) }}">
                    </div>

                    <div class="form-group">
                        <label>Last name <span class="asterisk">*</span> </label>
                        <input type="text" name="last_name" class="form-control"
                               value="{{ old( 'last_name', $customer->last_name) }}">
                    </div>

                    <div class="form-group">
                        <label>Street number <span class="asterisk">*</span> </label>
                        <input type="text" name="street_number" class="form-control"
                               value="{{ old( 'street_number', $customer->street_number) }}">
                    </div>

                    <div class="form-group">
                        <label>Street name <span class="asterisk">*</span> </label>
                        <input type="text" name="street_name" class="form-control"
                               value="{{ old( 'street_name', $customer->street_name) }}">
                    </div>

                    <div class="form-group">
                        <label>City <span class="asterisk">*</span> </label>
                        <input type="text" name="city" class="form-control" value="{{ old( 'city', $customer->city) }}">
                    </div>

                    <div class="form-group">
                        <label>Region name <span class="asterisk">*</span> </label>
                        <select name="region_name" class="form-control">
                            <option disabled="disabled" selected="selected">Region name</option>
                            <option value="{{ old( 'region_name', $customer->region_name) }}" selected="selected">
                                Quebec
                            </option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Postal code <span class="asterisk">*</span> </label>
                        <input type="text" name="postal_code" class="form-control"
                               value="{{ old( 'postal_code', $customer->postal_code) }}">
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone number <span class="asterisk">*</span> </label>
                        <input id="phone" type="text" class="form-control virgin" name="phone_number"
                               placeholder="+1(000)-000-0000"
                               value="{{ old( 'phone_number', $customer->phone_number) }}">
                    </div>

                    <div class="form-group">
                        <label for="package_status">Package status <span class="asterisk">*</span> </label>
                        <input id="package_status" type="text" class="form-control" name="package_status"
                               value="{{ old( 'package_status', $customer->package_status) }}">
                    </div>
                    <div class="form-group">
                        <label>Dsl eligible</label>
                        <br>
                        <select name="dsl_eligible" class="form-control">
                            <option value="Unknown" {{ old('dsl_eligible', 'Unknown') == $customer->dsl_eligible ? 'selected' : '' }}>
                                Dsl Eligible
                            </option>
                            <option value="yes" {{ old('dsl_eligible', 'yes') == $customer->dsl_eligible ? 'selected' : '' }}>
                                Yes
                            </option>
                            <option value="no" {{ old('dsl_eligible', 'no') == $customer->dsl_eligible ? 'selected' : '' }}>
                                No
                            </option>
                        </select>
                        @if ($errors->has('dsl_eligible'))
                            <span class="error inn-error">{{ $errors->first('dsl_eligible') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection



