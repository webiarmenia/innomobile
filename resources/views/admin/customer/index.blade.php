@extends('layouts.app')

@section('title')
    <title>Customers</title>
@endsection

@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                @if (\Session::has('success'))
                    <div class="alert alert-success fade-in" style="width: 100%">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {!! \Session::get('success') !!}
                    </div>
                @endif
                <h3 class="card-header text-center font-weight-bold text-uppercase py-44 wi-page-title"
                    id="active-customer-page">Customer table</h3>
                <div class="card-body">
                    <a href="{{ route('export') }}" class="btn btn-success">Export Customer Data</a>
                    <div id="table" class="table-editable" style="overflow-x: auto">
                        <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i
                                        class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Mail</th>
                                <th class="text-center">First Name</th>
                                <th class="text-center">Last Name</th>
                                <th class="text-center">Package of Interest</th>
                                <th class="text-center">Package Status</th>
                                <th class="text-center">Eligible for DSL</th>
                                <th class="text-center">Created At</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($customers as $customer)
                                <tr>
                                    <td class="pt-3-half">{{$customer->id}}</td>
                                    <td class="pt-3-half">{{$customer->email}}</td>
                                    <td class="pt-3-half">{{$customer->first_name}}</td>
                                    <td class="pt-3-half">{{$customer->last_name}}</td>
                                    <td class="pt-3-half">{{$customer->package_of_interest}}</td>
                                    <td class="pt-3-half">{{$customer->package_status}}</td>
                                    <td class="pt-3-half">{{$customer->dsl_eligible}}</td>
                                    <td class="pt-3-half">{{$customer->created_at->subHours(5)}}</td>
                                    <td class="text-center">
                                        <a href="{{ url('/innomobile_super_admin/customer/edit'.'/'.$customer->id.'/'.$customer->package->package_isp) }}"
                                           class="btn btn-success edit btn btn-sm btn-default"
                                           style="margin-top: 5px"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="{{ url('/innomobile_super_admin/customer/show'.'/'.$customer->id.'/'.$customer->package->package_isp) }}"
                                           class="btn btn-primary edit btn btn-sm btn-default"
                                           style="margin-top: 5px"><i class="fa fa-pencil"></i> View</a>
                                        <form action="{{route('admin.customer.destroy',$customer->id)}}" method="POST"
                                              style="display: inline-block;margin-bottom: 5px">
                                            @csrf
                                            <input type="hidden" name="type"
                                                   value="{{$customer->package->package_isp}}">
                                            <button type="submit" class="btn btn-danger edit btn btn-sm btn-default"
                                                    data-toggle="tooltip" data-placement="top"
                                                    style="margin-bottom:0; margin-top:5px">
                                                <i class="fa fa-times-circle"></i> Delete
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $customers->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
