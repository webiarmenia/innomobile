@extends('layouts.app')

@section('title')
    <title>Show customer -> {{ $customer->email }}</title>
@endsection

@section('css')
    <style>
        .col-md-6 {
            text-align: center !important;
        }
    </style>
@endsection
@section('content')
    <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4 wi-page-title"
            id="active-customer-page">Show customer</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                <div class="row ">
                    <div class="col-md-12">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-12" style="text-align: center;">
                                    <i class="fa fa-user f-80" style="font-size: 100px"></i>
                                </div>
                                <div class="col-md-12">
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Customer ID:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->id }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Email:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->email }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Language:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->language }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Title:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->title }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            First Name:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->first_name }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Last Name:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->last_name }}
                                        </div>
                                    </div>
                                    @if($customer->package->package_isp == 'Transat')
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Appartment Number:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->appartment_number }}
                                        </div>
                                    </div>
                                    @endif
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Street Number:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->street_number }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Street Name:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->street_name }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            City:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->city }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Province:
                                        </div>
                                        <div class="col-md-6">
                                            Quebec
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Postal Code:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->postal_code }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Phone Number:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->phone_number }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Package of Interest:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->package_of_interest }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Package Status:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->package_status }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Eligible for DSL:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->dsl_eligible }}
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 10px;">
                                        <div class="col-md-6">
                                            Created At:
                                        </div>
                                        <div class="col-md-6">
                                            {{ $customer->created_at->subHours(5) }}
                                        </div>
                                    </div>

                                    @if($customer->Transat != null)
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Date approximative:
                                            </div>
                                            <div class="col-md-6">
                                                {{ $customer->Transat->daterendivo }}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Période d'installation souhaitée:
                                            </div>
                                            <div class="col-md-6">
                                                @if($customer->Transat->priode == 2)
                                                    Matin (7h30 - midi)
                                                @elseif($customer->Transat->priode == 3)
                                                    Apres-midi (midi - 17h)
                                                @elseif($customer->Transat->priode == 4)
                                                    Soir (17h - 21h)
                                                @else
                                                    Jour entier (7h30 - 21h)
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Date approximative (1):
                                            </div>
                                            <div class="col-md-6">
                                                {{ $customer->Transat->daterendivo1 }}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Période d'installation souhaitée (1):
                                            </div>
                                            <div class="col-md-6">
                                                @if($customer->Transat->priode1 == 2)
                                                    Matin (7h30 - midi)
                                                @elseif($customer->Transat->priode1 == 3)
                                                    Apres-midi (midi - 17h)
                                                @elseif($customer->Transat->priode1 == 4)
                                                    Soir (17h - 21h)
                                                @else
                                                    Jour entier (7h30 - 21h)
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Date approximative (2):
                                            </div>
                                            <div class="col-md-6">
                                                {{ $customer->Transat->daterendivo2 }}
                                            </div>
                                        </div>
                                        <div class="row" style="margin-bottom: 10px;">
                                            <div class="col-md-6">
                                                Période d'installation souhaitée (2):
                                            </div>
                                            <div class="col-md-6">
                                                @if($customer->Transat->priode2 == 2)
                                                    Matin (7h30 - midi)
                                                @elseif($customer->Transat->priode2 == 3)
                                                    Apres-midi (midi - 17h)
                                                @elseif($customer->Transat->priode2 == 4)
                                                    Soir (17h - 21h)
                                                @else
                                                    Jour entier (7h30 - 21h)
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



