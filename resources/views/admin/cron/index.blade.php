@extends('layouts.app')

@section('title')
    <title>Cron jobs</title>
@endsection

@section('content')
    <div class="container">
        <div class="justify-content-center">
            <div class="card">
                <h3 class="card-header text-center font-weight-bold text-uppercase py-44 wi-page-title" id="active-cron-page">Cron Job table</h3>
                <div class="card-body">
                    <form id="form1" class="form-horizontal" action="{{route('admin.cron.index')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <select name="those_worked_time" class="those_worked_time form-control">
                                <option value="" {{ $selected == 10 ? 'selected': '' }}>All</option>
                                <option value="0" {{ $selected == 0 ? 'selected': '' }}>Primary</option>
                                <option value="1"{{ $selected == 1 ? 'selected': '' }}>Success</option>
                                <option value="2" {{ $selected == 2 ? 'selected': '' }}>Danger</option>
                            </select>
                        </div>
                        <div>
                            <div class="float-right">
                                <button id="submit" type="submit" class="btn btn-primary m-t-10" value="filter" name="button"><i class="fa fa-check"></i> Посмотреть</button>
                            </div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                    <div id="table" class="table-editable">
                        <span class="table-add float-right mb-3 mr-2"><a href="#!" class="text-success"><i class="fas fa-plus fa-2x" aria-hidden="true"></i></a></span>
                        <table class="table table-bordered table-responsive-md table-striped text-center">
                            <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Customer</th>
                                <th class="text-center">Worked time</th>
                                <th class="text-center">Updated time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($cronJobs as $cronJob)
                                <tr>
                                    <td class="pt-3-half" >{{$cronJob->id}}</td>
                                    <td class="pt-3-half" >{{$cronJob->status}}</td>
                                    <td class="pt-3-half" ><a href="{{ url('/innomobile_super_admin/customer/edit'.'/'.$cronJob->customer_id.'/'.$cronJob->customer->package->package_isp) }}">{{$cronJob->customer->email}}</a></td>
                                    <td class="pt-3-half" >
                                        @if($cronJob->worked_time == 0)
                                            <input class="btn btn-primary submit_none" style="padding: 4px 25px;" type="submit" id="{{ $cronJob->id }}"  value="{{$cronJob->worked_time}}">
                                        @elseif($cronJob->worked_time == 1)
                                            <input class="btn btn-success submit_none" style="padding: 4px 25px;" type="submit" id="{{ $cronJob->id }}"  value="{{$cronJob->worked_time}}">
                                        @else
                                            <input class="btn btn-danger submit" style="padding: 4px 25px;" type="submit" id="{{ $cronJob->id }}" value="{{$cronJob->worked_time}}">
                                        @endif
                                    </td>
                                    <td class="pt-3-half" >{{$cronJob->updated_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $cronJobs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".submit_none").click(function(){
                alert('You can only update the column whose -WORKED TIME- is equal to 2');
            });
            $(".submit").click(function(){
                $id = $(this).attr('id');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "cron",
                    method: "POST",
                    data: {id:$id},
                    success: function () {
                        function refresh(){
                            location = ''
                        }
                        refresh();
                    }
                })
            });
        });
    </script>
@endsection

