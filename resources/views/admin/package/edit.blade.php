@extends('layouts.app')

@section('title')
    <title>Customer package</title>
@endsection('title')

@section('content')
    <div class="card">
        <h3 class="card-header text-center font-weight-bold text-uppercase py-4 wi-page-title"
            id="active-package-page">Update package</h3>
        <div class="card-body">
            <div id="table" class="table-editable">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{ route('admin.package.update', $package->id) }}"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    @method('PUT')

                    <div class="form-group">
                        <label>Package Id <span class="asterisk">*</span> </label>
                        <input type="text" name="package_id" class="form-control"
                               value="{{ old( 'package_id', $package->package_id) }}" readonly>
                    </div>

                    <div class="form-group">
                        <label>Package Name <span class="asterisk">*</span> </label>
                        <input type="text" name="package_name" class="form-control"
                               value="{{ old( 'package_name', $package->package_name) }}" readonly>
                    </div>

                    <div class="form-group">
                        <label>Package Type <span class="asterisk">*</span> </label>
                        <input type="text" name="package_type" class="form-control"
                               value="{{ old( 'package_type', $package->package_type) }}" readonly>
                    </div>

                    <div class="form-group">
                        <label>Package Isp <span class="asterisk">*</span> </label>
                        <input type="text" name="package_isp" class="form-control"
                               value="{{ old( 'package_isp', $package->package_isp) }}" readonly>
                    </div>

                    <div class="form-group">
                        <label>Package Price <span class="asterisk">*</span> </label>
                        <input type="number" name="package_price" class="form-control" step="any"
                               value="{{ old( 'package_price', $package->package_price) }}">
                    </div>

                    <div class="form-group">
                        <label>Currency <span class="asterisk">*</span> </label>
                        <input type="text" name="currency" class="form-control"
                               value="{{ old( 'currency', $package->currency) }}">
                    </div>

                    @if($package->package_isp === "Virgin")
                        <div class="form-group">
                            <label>Package Email <span class="asterisk">*</span> </label>
                            <input type="email" name="package_email" class="form-control"
                                   value="{{ old( 'package_email', $package->package_email) }}">
                        </div>
                    @endif

                    <div class="form-group @if($package->package_isp === "Virgin")d-none @endif">
                        <label>Login <span class="asterisk">*</span> </label>
                        <input type="text" name="login" class="form-control"
                               value="{{ old( 'login', $package->login) }}">
                    </div>

                    <div class="form-group @if($package->package_isp === "Virgin")d-none @endif">
                        <label>Password <span class="asterisk">*</span> </label>
                        <input type="text" name="password" class="form-control"
                               value="{{ old( 'password', $package->password) }}">
                    </div>

                    <div class="form-group">
                        <input type="checkbox" name="selected" value="1"
                            {{ old( 'selected', $package->selected) ? ' checked': '' }}>
                        <label>Show this as a selected package in form </label>
                    </div>

                    <div class="form-group">
                        <br>
                        <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
            var packageId = $('input[name="package_id"]');
            var packageIdArray = packageId.val().split('_');
            $('input[name="package_price"]').change(function(){
                var newPriceArray = $(this).val().split('.');
                packageId.val(packageId.val().replace(packageIdArray[3], newPriceArray[0]));
                packageIdArray[3] = newPriceArray[0];
            });
        });
    </script>
@endsection



