@extends('layouts.layouts')

@section('content')
    @if (\Session::has('success'))
        <div class="alert alert-success fade-in" style="width: 100%">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! \Session::get('success') !!}
        </div>
    @endif
    @if (\Session::has('error'))
        <div class="alert alert-danger fade-in" style="width: 100%">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {!! \Session::get('error') !!}
        </div>
    @endif
@endsection